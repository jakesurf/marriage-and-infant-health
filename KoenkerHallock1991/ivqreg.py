""" Quantile Instrumental Variables Regression - Alex Poulsen """

# Import data


n = X.shape[0]


# Quantile IV
def QTreg_solve(betaguess, tau, Y, X):
    u = Y - (np.dot(X,betaguess)).reshape(n,1)
    # here is the tilted absolute value function
    u_pos = np.where(u>0)[0]
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    return rho_pos.sum() + np.abs(rho_neg.sum())

# THIS ISN'T RIGHT - look at KoenkerHallock1991v4.py
def IVQTreg(betaguess, tau, Y, X, Z):
    # 1st Stage
    k = X.shape[1]
    delta = np.zeros(k)
    success = np.zeros(k)
    for i in range(1,k)
        deltaguess = np.array([1,1])
        newX = np.array([np.ones(n),X[:,i]])
        QTsolve = lambda deltaguess: QTreg_solve(deltaguess, tau, newX, Z)
        powell = spicy.minimize(QTsolve,deltaguess,method='Powell')
        delta[i] = powell.x[1]
        success[i] = powell.success
    delta = delta.reshape(k,1)
    # 2nd Stage
    Xhat = np.dot(Z,delta) 
    QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, Xhat)
    powell = spicy.minimize(QTsolve,betaguess,method='Powell')
    beta = powell.x
    return beta