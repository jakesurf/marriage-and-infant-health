""" Replication of Koenker & Hallock (1991) - Alex Poulsen

This version includes a Quantile Instrumental Variables Estimator

"""

import numpy as np
from numpy import linalg
import scipy.optimize as spicy
from pandas import Series, DataFrame, Index 
import pandas as pd 
import os 
import time
from matplotlib import pyplot as plt

os.chdir("/Users/Alex/Economics/KoenkerHallock1991")
 
# Import the data
data = pd.io.parsers.read_table("marr_notcollapse_forpython.txt")

data = data.dropna()

#################### OLS ##########################
n = data.shape[0]
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
Y = np.array(data.dbirwt).reshape(n,1)
a = linalg.inv(np.dot(X.T,X))
#b = np.dot(X.T,Y)
#beta = np.dot(a,b)
betaguess = np.array([2990,70,-213,-131,15,15,0,-112])

def OLS(betaguess, X,Y):
    return ((Y - np.dot(X,betaguess).reshape(n,1))**2).sum()

OLSsolve = lambda betaguess: OLS(betaguess, X, Y)

betaOLS = spicy.fmin(OLSsolve,betaguess)


uhat = Y-betaOLS[0]-(betaOLS[1]*X[:,1]).reshape(n,1)
sigma2 = (1./(n-2))*np.dot(uhat.T,uhat)
varcovar = sigma2[0,0]*a

print "Using my python code, I get the following results for OLS: "
print "b0 = ", betaOLS[0], "/ s.e.(b0) = ", varcovar[0,0]**.5
print "b1 = ", betaOLS[1], "/ s.e.(b1) = ", varcovar[1,1]**.5
print "b2 = ", betaOLS[2], "/ s.e.(b2) = ", varcovar[2,2]**.5
print "b3 = ", betaOLS[3], "/ s.e.(b3) = ", varcovar[3,3]**.5
print "b4 = ", betaOLS[4], "/ s.e.(b4) = ", varcovar[4,4]**.5
print "b5 = ", betaOLS[5], "/ s.e.(b5) = ", varcovar[5,5]**.5
print "b6 = ", betaOLS[6], "/ s.e.(b6) = ", varcovar[6,6]**.5
print "b7 = ", betaOLS[7], "/ s.e.(b7) = ", varcovar[7,7]**.5

reg = pd.ols(y = data.dbirwt, x = data[['married','black','otherrace','dmeduc','dmage','dmage2','female']])
print "Pandas' built in OLS function gives me: "
print reg


################## Quantile Regression #####################
n = data.shape[0]
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
Y = np.array(data.dbirwt).reshape(n,1)

# have the guesses for beta be the OLS estimates
betaguess = beta

def QTreg_solve(betaguess, tau, Y, X):
    u = Y - (np.dot(X,betaguess)).reshape(n,1)
    # here is the tilted absolute value function
    u_pos = np.where(u>0)[0]
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    return rho_pos.sum() + np.abs(rho_neg.sum())

########################################################################

# Graph of each coefficient by quantile

nq = 19
quantiles = np.linspace(.05,.95,nq)
coeffs = np.zeros((nq,len(betaguess)))
success = np.zeros(nq)

for i in range(nq):
    tau = quantiles[i]
    QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)
    powell = spicy.minimize(QTsolve,betaguess,method='Powell')
    #beta_min = spicy.fmin(QTsolve,betaguess)
    coeffs[i] = powell.x #beta_min
    success[i] = powell.success

#graph
variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female']
for i in range(8):    
    plt.plot(quantiles,coeffs[:,i])
    plt.title("Coeff. of "+variables[i]+" by quantile")
    plt.savefig("byQuantile_"+variables[i]+".png")
    plt.show()

print success


########################################################################

### Quantile Instrumental Variables Regression

Z = np.array([np.ones(n),np.array(data.bldtest),black,otherrace,dmeduc,dmage,dmage2,female]).T

def IVQTreg(betaguess, tau, Y, X, Z):
    # 1st Stage
    k = X.shape[1]
    delta = np.zeros((k,k))
    success = np.zeros(k+1)
    for i in range(k):
        deltaguess = np.ones(k)# there has to be a better way to do these guesses
        newX = X[:,i].reshape(n,1)
        QTsolve = lambda deltaguess: QTreg_solve(deltaguess, tau, newX, Z)
        powell = spicy.minimize(QTsolve,deltaguess,method='Powell')
        delta[:,i] = powell.x
        success[i] = powell.success
    #delta = delta.reshape(k,1)
    
    # 2nd Stage
    Xhat = np.dot(Z,delta) 
    QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, Xhat)
    powell = spicy.minimize(QTsolve,betaguess,method='Powell')
    beta = powell.x
    success[-1] = powell.success
    
    return beta,success

##### Now, more analysis with some cool graphs #####


coeffsIVQ = np.zeros((nq,len(betaguess)))
successIVQ = np.zeros((nq,k+1))

t1 = time.time()
for i in range(nq):
    tau = quantiles[i]
    #QTsolve = lambda betaguess: IVQTreg(betaguess, tau, Y, X, Z)
    #powell = spicy.minimize(QTsolve,betaguess,method='Powell')
    ivqreg = IVQTreg(betaguess, tau, Y, X, Z)
    coeffsIVQ[i] = ivqreg[0]
    successIVQ[i] = ivqreg[1]

#graph
variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female']
for i in range(8):    
    plt.plot(quantiles,coeffsIVQ[:,i])
    plt.title("Coeff. of "+variables[i]+" by quantile (from Quantile IV Regression)")
    plt.savefig("byIVQuantile_"+variables[i]+".png")
    plt.show()

print successIVQ
totalt = time.time() - t1

"""I am going to estimate that this will take about 40 minutes, lets see..."""
print "This took ", totalt/60, " minutes"



