""" Replication of Koenker & Hallock (1991) 

- This version (v3) has an investigation of why it takes forever for 
the quantile regression to converge when you use lots
of fixed effects dummy variables, wheras v2 has the graphs of coeffs
by quantile, without the fixed effects.

Written by Alex Poulsen """


import numpy as np
from numpy import linalg
import scipy.optimize as spicy
from pandas import Series, DataFrame, Index 
import pandas as pd 
import os 
import time
import sys
import random

os.chdir("/Users/Alex/Economics/KoenkerHallock1991")

# change the output directory
orig_stdout = sys.stdout
f = file("KH1991v3_2.txt","w")
sys.stdout = f



 
# Import the data
data = pd.io.parsers.read_table("marr_notcollapse_forpython.txt")
# get rid of nanners
data = data.dropna()

# Pare down the data a bit more (10% sample)
rows = random.sample(data.index,data.shape[0]/10)
data = data.ix[rows]

# creating dummy variables for year and state
statedummies = pd.core.reshape.get_dummies(data['state'])
yeardummies = pd.core.reshape.get_dummies(data['year'])
data = data.join(statedummies)
data = data.join(yeardummies)

data['time'] = data['year']-1968

#################### OLS ##########################
n = data.shape[0]
# Converting all of the data fom pandas objects to numpy arrays
# Here are each of the important variables
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
time = np.array(data.time).reshape(n,1)
# Here are all of the dummy variables - so many!!
statedum = np.array(statedummies)
yeardum = np.array(yeardummies)
state_timedum = statedum*time
state_time2dum = statedum*(time)**2
# now putting them all together
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
X = np.concatenate((X,statedum[:,1:]), axis=1)
X = np.concatenate((X,yeardum[:,1:]), axis=1)
X = np.concatenate((X,state_timedum[:,1:]), axis=1)
X = np.concatenate((X,state_time2dum[:,1:]), axis=1)

Y = np.array(data.dbirwt).reshape(n,1)
a = linalg.inv(np.dot(X.T,X))
#b = np.dot(X.T,Y)
#beta = np.dot(a,b)
betaguess = np.array([2990,70,-213,-131,15,15,0,-112])
dumguess = np.zeros(X.shape[1]-8)
betaguess = np.concatenate((betaguess,dumguess),axis=1)

def OLS(betaguess, X,Y):
    return ((Y - np.dot(X,betaguess).reshape(n,1))**2).sum()

OLSsolve = lambda betaguess: OLS(betaguess, X, Y)

beta = spicy.fmin(OLSsolve,betaguess)


uhat = Y-beta[0]-(beta[1]*X[:,1]).reshape(n,1)
sigma2 = (1./(n-2))*np.dot(uhat.T,uhat)
varcovar = sigma2[0,0]*a

print "Using my python code, I get the following results for OLS: "
print "b0 = ", beta[0], "/ s.e.(b0) = ", varcovar[0,0]**.5
print "b1 = ", beta[1], "/ s.e.(b1) = ", varcovar[1,1]**.5
print "b2 = ", beta[2], "/ s.e.(b2) = ", varcovar[2,2]**.5
print "b3 = ", beta[3], "/ s.e.(b3) = ", varcovar[3,3]**.5
print "b4 = ", beta[4], "/ s.e.(b4) = ", varcovar[4,4]**.5
print "b5 = ", beta[5], "/ s.e.(b5) = ", varcovar[5,5]**.5
print "b6 = ", beta[6], "/ s.e.(b6) = ", varcovar[6,6]**.5
print "b7 = ", beta[7], "/ s.e.(b7) = ", varcovar[7,7]**.5

reg = pd.ols(y = data.dbirwt, x = data[['married','black','otherrace','dmeduc','dmage','dmage2','female']])
print "Pandas' built in OLS function gives me: "
print reg


################## Quantile Regression #####################

# have the guesses for beta be the OLS estimates
betaguess = beta
iterat = 1
funcVals = list()

def QTreg_solve(betaguess, tau, Y, X):
    u = Y - (np.dot(X,betaguess)).reshape(n,1)
    # here is the tilted absolute value function
    u_pos = np.where(u>0)[0]
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    return rho_pos.sum() + np.abs(rho_neg.sum())

tau = .5
QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)
#beta_min = spicy.fmin(QTsolve,betaguess)

def callbackFunction(Xi):
    #global iterat
    global funcVals
    funcVals.append(QTsolve(Xi))
    #print "Iteration ", iterat ,": f(x) = ", QTsolve(Xi) 
    #iterat += 1


#t1 = time.time()
beta_min = spicy.minimize(QTsolve,betaguess,callback=callbackFunction, method='L-BFGS-B',tol=1e-7)
# beta_min = spicy.minimize(QTreg_solve,betaguess,args=(tau,Y,X),method = "Nelder-Mead")
#totaltime = time.time()-t1
print beta_min
#print "This took ", totaltime/60, " minutes."



########################################################################
# Making a graph to see what is going on.
# The objective function as a funciton of one thing only, holding others constant
from matplotlib import pyplot as plt

os.chdir("/Users/Alex/Economics/KoenkerHallock1991/objfunc_OLSest")

tau = .5
def QTobjectiveF(var, i):
    betaguess[i] = var
    return QTreg_solve(betaguess, tau, Y, X)


# First, graphing the objective function from the OLS estimates
k = 0
for i in [0,1,2,3,4,5,6,7,8,9,58,59,93,94,143,144]:
    betaguess = beta.copy()
    xval = betaguess[i].copy()

    m = 151 # grid size
    offs = 1
    of = 25
    grid = np.zeros(m)
    if i < 8:
        grid = np.linspace(betaguess[i]-offs*betaguess[i],betaguess[i]+offs*betaguess[i],m)
    if i >= 8:
        grid = np.linspace(betaguess[i]-of,betaguess[i]+of,m)
    y = np.zeros(m)
    for j in range(m):
        y[j] = QTobjectiveF(grid[j],i)

    variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female','state_2','state_3','year_2','year_3','state_time_2','state_time_3','state_time2_2','state_time2_3']
    plt.plot(grid,y)
    plt.plot(xval,y.min(),"ro")
    plt.title(variables[k])
    plt.savefig("objfuncfromOLSest_"+variables[k]+".png")
    plt.show()
    k += 1


# Now, graphing with the right estimates, but ver up close with big grid
os.chdir("/Users/Alex/Economics/KoenkerHallock1991/objfunc_QTbiggrid")

k = 0
for i in [0,1,2,3,4,5,6,7,8,9,10,58,59,60,93,94,95,143,144,145]: 
    betaguess = beta_min.x.copy()
    xval = betaguess[i].copy()

    m = 500 # grid size
    offs = .5
    of = 5
    grid = np.zeros(m)
    if i < 8:
        grid = np.linspace(betaguess[i]-offs*betaguess[i],betaguess[i]+offs*betaguess[i],m)
    if i >= 8:
        grid = np.linspace(betaguess[i]-of,betaguess[i]+of,m)
    y = np.zeros(m)
    for j in range(m):
        y[j] = QTobjectiveF(grid[j],i)

    variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female','state_2','state_3','state_4','year_2','year_3','year_4','state_time_2','state_time_3','state_time_4','state_time2_2','state_time2_3','state_time2_4']
    plt.plot(grid,y)
    plt.plot(xval,y.min(),"ro")
    plt.title(variables[k])
    plt.savefig("objfuncfromQTest_"+variables[k]+".png")
    plt.show()
    k += 1

xgrid = range(len(funcVals))
plt.plot(xgrid,funcVals)
plt.title('Objective function value')
plt.savefig('funcValue')
plt.show()


# change back the output directory
sys.stdout = orig_stdout
f.close()








os.chdir("/Users/Alex/Economics/KoenkerHallock1991/objfuncstates")

k = 0
for i in range(20,58):
    betaguess = beta_min.x.copy()
    xval = betaguess[i].copy()

    m = 500 # grid size
    offs = .5
    of = 5
    grid = np.zeros(m)
    if i < 8:
        grid = np.linspace(betaguess[i]-offs*betaguess[i],betaguess[i]+offs*betaguess[i],m)
    if i >= 8:
        grid = np.linspace(betaguess[i]-of,betaguess[i]+of,m)
    y = np.zeros(m)
    for j in range(m):
        y[j] = QTobjectiveF(grid[j],i)

    #variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female','state_2','state_3','state_4','year_2','year_3','year_4','state_time_2','state_time_3','state_time_4','state_time2_2','state_time2_3','state_time2_4']
    plt.plot(grid,y)
    plt.plot(xval,y.min(),"ro")
    plt.title("State " +str(i-6)+" Fixed FX")
    plt.savefig("objfuncfromQTest_state"+str(i-6)+".png")
    #plt.show()
    plt.cla()
    k += 1



















































import sys

orig_stdout = sys.stdout
f = file("output.txt","w")
sys.stdout = f

a = np.array([[1,2,3],[4,5,6],[7,8,9]])
b = np.array([[2],[2],[2]])

print np.dot(a,b)

sys.stdout = orig_stdout
f.close()




np.concatenate((a,b),axis=1)


a = np.array([[1,1,1],[1,1,1],[2,1,1]])
b = np.array([[4],[2],[2]])
d= np.array([1,2,3])
c = np.delete(a,2,1)


# from fmin
betaguess = np.array([ 3.26171558e+03,   6.54789830e+01,  -1.85955923e+02,
        -1.57159429e+02,   1.53029929e+01,  -3.61769987e+00,
         4.41789475e-03,  -1.18288286e+02,  -1.03053576e-04,
        -9.19136213e-05,   8.56122186e-04,  -2.79599426e-03,
         6.54883285e-04,   1.74085184e-04,  -3.18136154e-04,
         2.23237478e-04,  -3.54059371e-05,   1.66964862e-02,
         1.89343216e-02,  -4.25271534e-03,  -4.96674747e-04,
        -4.58057546e-03,  -1.04298559e-02,  -2.79178823e-03,
        -7.60431013e-03,  -2.54847151e-03,  -8.76585132e-03,
         1.04873109e-03,  -3.69399020e-04,   3.75539195e-06,
        -7.18147783e-04,  -4.10076469e-04,  -1.16120440e-03,
        -4.07259790e-04,  -5.99404457e-03,  -5.50660461e-04,
        -2.71665203e-03,   1.26472396e-03,  -3.53578735e-03,
         6.26158308e-05,  -1.05969301e-03,  -1.13032947e-03,
        -1.26651127e-03,  -1.33535090e-03,   2.30536124e-03,
        -1.02947023e-03,  -3.13064965e-03,  -8.61571249e-04,
        -1.01488153e-02,  -7.70862916e-03,   2.30640699e-04,
         5.61179882e-04,  -4.00240922e-03,  -5.23106057e-03,
         1.97991025e-03,  -6.53678375e-03,  -2.14095667e-03,
         1.85894299e-03,  -1.12281091e-03,   2.91565194e-03,
         4.99125693e-04,  -1.53993420e-02,   2.10804216e-02,
         3.27696089e-02,   1.10905913e-03,   2.31865403e-03,
        -9.38641277e-05,  -1.43994239e-03,  -6.05148649e-03,
        -2.13892977e-04,  -1.93239909e-03,  -2.64605644e-04,
         2.02636848e-04,  -1.93264943e-02,   8.45338037e-03,
        -6.57828656e-04,  -6.91256504e-03,  -3.52017684e-04,
         3.11872300e-03,  -3.34135911e-05,  -5.31851717e-05,
         2.23110232e-04,  -2.45059926e-03,  -2.45141562e-04,
        -8.05536527e-07,   6.79722414e-03,  -2.98105049e-04,
        -4.79173022e-04,  -4.39374568e-04,   5.78912955e-04,
         1.54144115e-03,   3.00779760e-03,   3.88046647e-04,
        -2.97619939e-03,   1.07424866e-03,   7.27662544e-06,
        -4.81324447e-04,   4.84112692e-04,   4.40575382e-04,
         1.03736919e-03,  -7.23435366e-04,   5.80283923e-04,
        -4.04488311e-04,   3.07421406e-04,   2.69415541e-04,
        -2.05721293e-03,   1.83656075e-04,   7.22152484e-04,
        -1.04094240e-03,   1.81124648e-04,  -7.46366198e-05,
         2.24500429e-04,   1.07149738e-05,  -6.43915452e-06,
        -1.77006560e-03,  -2.38590534e-05,   1.28227278e-03,
        -1.31046393e-04,   5.34504653e-05,  -2.28266275e-03,
        -2.22029095e-05,   4.88055508e-04,   4.50984593e-04,
         9.93673222e-04,   1.06864529e-03,  -1.73493721e-03,
         2.82827815e-03,  -2.75024992e-05,   7.70702211e-04,
        -1.81544816e-04,  -1.97806806e-05,   1.30707586e-03,
        -7.74867127e-05,   1.15802141e-04,   5.19768205e-04,
        -1.23022065e-04,   1.77114871e-03,   3.74674374e-04,
        -2.40291215e-04,   1.02008754e-03,  -9.79397754e-04,
        -1.53716915e-03,  -1.63250186e-04,   5.93740856e-05,
        -5.58848176e-04,  -4.12504152e-03,   5.72342713e-03,
        -3.24894308e-03,  -4.89976599e-04,   4.20699287e-04,
        -1.17970607e-04,  -1.44367471e-03,  -1.15150091e-03,
        -3.51813267e-04,   1.00639425e-03,  -6.69951231e-04,
        -6.82112680e-04,   4.37754748e-03,  -3.12846637e-03,
        -1.44735813e-03,  -4.93842854e-03,  -6.24885057e-04,
         3.13301979e-03,   6.53554609e-04,   7.49606826e-04,
         5.92399513e-04,  -6.40952948e-04,  -8.18365521e-04,
         7.77508464e-04,  -2.49137591e-04,   5.79812008e-04,
         8.19608236e-05,   2.41522269e-03,  -2.49590029e-04,
         7.41919163e-03,   1.42761129e-03,  -8.61240409e-04,
        -3.11026495e-03,   3.01984314e-03,   1.08202467e-01,
         5.27953992e-05,   7.73215469e-04,  -1.38305931e-05,
        -3.25599982e-04,   2.56293272e-04,  -1.15711604e-02,
         4.75573415e-03,  -1.91857056e-04,   1.81195167e-03,
         1.09822743e-03,  -1.84599419e-04,   1.50669366e-02,
        -1.21261119e-05])


# from powell - this one took forever and a half
newbeta = np.array([  3.21021646e+03,   6.88193395e+01,  -1.88714162e+02,
        -1.59898879e+02,   1.59967925e+01,  -3.16618479e+00,
        -2.06853527e-02,  -1.18655760e+02,   3.48225168e+00,
        -8.27017465e+01,  -8.76037287e+01,  -2.74353729e+01,
        -1.90486622e+02,  -5.44555963e+01,  -4.42498610e+01,
        -4.70525436e+01,  -4.17628119e+01,   8.22155734e+00,
        -1.26173962e+01,  -1.02423429e+02,  -4.35112328e+01,
        -5.65837083e+01,   1.26635552e+01,  -1.75127290e+01,
        -2.48894194e+01,  -8.49455247e+01,  -3.76351660e+01,
         6.05967669e+00,  -2.61853273e+01,  -5.68319320e+01,
         1.14126866e+01,  -7.14836652e+00,  -9.35941319e+00,
        -1.42293307e+01,  -5.74255674e+01,   3.42178980e+00,
        -3.55253356e+01,  -7.08755289e+01,  -1.65170813e+02,
        -7.34038223e+01,  -1.34152258e+01,  -1.65752750e+02,
        -5.35986399e+01,  -3.02983873e+01,   1.27316527e+01,
        -5.07975820e+01,  -1.91181978e+01,  -2.90016025e+01,
        -6.06112842e+01,  -4.17008463e+01,  -7.26559174e+01,
        -1.12787227e+02,  -1.97858150e+01,  -5.22966076e+01,
         9.87692261e+01,  -2.09140005e+01,  -2.53729491e+01,
        -2.21566247e+02,   3.06167147e+01,   2.82936725e+01,
         1.80806318e+01,   4.25836489e+01,   5.26557075e+01,
         5.19602861e+01,   5.12989354e+01,   4.92567641e+01,
         7.45966451e+01,   6.67575766e+01,   8.17509174e+01,
         7.97643920e+01,   8.60521086e+01,   8.74242140e+01,
         7.91348634e+01,   8.65697269e+01,   8.41152850e+01,
         8.54462221e+01,   8.40231676e+01,   9.78279214e+01,
         9.06799431e+01,   8.90127798e+01,   7.66265850e+01,
         7.72926768e+01,   6.95471023e+01,   6.38623771e+01,
         6.63523733e+01,   6.87370317e+01,   5.55984130e+01,
         5.86978357e+01,   5.72457204e+01,   4.23668085e+01,
         4.08734435e+01,   6.89314490e+01,   1.89798423e+01,
         3.61797738e+00,   2.10184629e+00,   5.05547551e+00,
        -2.12399010e-01,  -2.41174979e+00,   1.70794077e+00,
         1.32479523e+00,   1.60227755e+00,   1.55489668e+00,
        -2.51245675e+00,  -4.45792550e+00,   4.26296397e+00,
         1.19349889e+00,   4.01809756e+00,  -7.02965585e-01,
        -1.37762050e+00,  -1.87660545e+00,   6.71692113e+00,
         2.29389906e+00,  -2.13895025e+00,  -2.98983907e-01,
         2.76951474e+00,  -3.68415463e+00,  -1.72879345e+00,
        -1.91588789e+00,  -1.65621522e+00,   1.78752257e+00,
        -4.60363209e-01,   3.79173799e+00,   2.46017329e+00,
        -2.05458877e+00,   1.45753813e+00,  -2.43017462e+00,
         2.06626575e+01,   5.17048489e-01,  -7.40288391e-01,
         3.28678888e+00,  -9.59107114e-01,  -1.92968064e+00,
        -1.79934525e-01,   3.30219141e+00,   2.23168425e-01,
         1.67143943e-02,   2.58258759e+00,  -5.68182810e-01,
         4.11202791e+00,  -4.50227943e+00,  -3.66678396e+00,
         5.58944386e-01,   6.56043927e+00,  -4.60667006e-02,
        -1.31847247e-02,  -8.43708749e-02,   5.02907792e-02,
         1.40658260e-01,   5.23245428e-03,  -6.65053920e-03,
         9.48486729e-03,  -1.91130221e-02,   5.81904700e-02,
         1.27140947e-01,  -7.05485304e-02,  -8.45317821e-04,
        -1.05276147e-01,   6.60631034e-04,   1.93528686e-02,
         8.50497842e-02,  -1.76037157e-01,  -8.89380637e-03,
         6.02179293e-02,   4.29909815e-02,  -2.27176928e-02,
         1.38751090e-01,   6.66828837e-02,   5.12117622e-02,
         4.91721485e-03,   6.50615824e-03,  -4.87173507e-02,
        -4.51801550e-02,  -1.63686027e-02,   1.42449479e-01,
         3.12055327e-02,   8.90770284e-02,  -5.21353485e-01,
         2.21207722e-02,   5.79321725e-02,  -5.44835193e-02,
         7.46311518e-02,   1.17446466e-01,  -5.10108502e-04,
        -1.34390465e-02,   1.46802701e-02,   3.44678357e-02,
        -5.56390271e-02,   1.22556481e-01,  -6.65742027e-02,
         9.76272561e-02,   1.06518121e-01,   1.98790078e-02,
        -1.27998287e-01])