""" Replication of Koenker & Hallock (1991) - Alex Poulsen
- This version has the graphs of coefficients by quantile,
  while v3 has the graphs of the objective function with
  all of the dummy variables

 """


import numpy as np
from numpy import linalg

import scipy.optimize as spicy

from pandas import Series, DataFrame, Index 
import pandas as pd 

import os 

os.chdir("/Users/Alex/Economics/KoenkerHallock1991")
 
# Import the data
data = pd.io.parsers.read_table("marr_notcollapse_forpython.txt")

data = data.dropna()

#################### OLS ##########################
n = data.shape[0]
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
Y = np.array(data.dbirwt).reshape(n,1)
a = linalg.inv(np.dot(X.T,X))
#b = np.dot(X.T,Y)
#beta = np.dot(a,b)
betaguess = np.array([2990,70,-213,-131,15,15,0,-112])

def OLS(betaguess, X,Y):
    return ((Y - np.dot(X,betaguess).reshape(n,1))**2).sum()

OLSsolve = lambda betaguess: OLS(betaguess, X, Y)

betaOLS = spicy.fmin(OLSsolve,betaguess)


uhat = Y-betaOLS[0]-(betaOLS[1]*X[:,1]).reshape(n,1)
sigma2 = (1./(n-2))*np.dot(uhat.T,uhat)
varcovar = sigma2[0,0]*a

print "Using my python code, I get the following results for OLS: "
print "b0 = ", betaOLS[0], "/ s.e.(b0) = ", varcovar[0,0]**.5
print "b1 = ", betaOLS[1], "/ s.e.(b1) = ", varcovar[1,1]**.5
print "b2 = ", betaOLS[2], "/ s.e.(b2) = ", varcovar[2,2]**.5
print "b3 = ", betaOLS[3], "/ s.e.(b3) = ", varcovar[3,3]**.5
print "b4 = ", betaOLS[4], "/ s.e.(b4) = ", varcovar[4,4]**.5
print "b5 = ", betaOLS[5], "/ s.e.(b5) = ", varcovar[5,5]**.5
print "b6 = ", betaOLS[6], "/ s.e.(b6) = ", varcovar[6,6]**.5
print "b7 = ", betaOLS[7], "/ s.e.(b7) = ", varcovar[7,7]**.5

reg = pd.ols(y = data.dbirwt, x = data[['married','black','otherrace','dmeduc','dmage','dmage2','female']])
print "Pandas' built in OLS function gives me: "
print reg


################## Quantile Regression #####################
n = data.shape[0]
married = np.array(data.married)
black = np.array(data.black)
otherrace = np.array(data.otherrace)
dmeduc = np.array(data.dmeduc)
dmage = np.array(data.dmage)
dmage2 = np.array(data.dmage2)
female = np.array(data.female)
X = np.array([np.ones(n),married,black,otherrace,dmeduc,dmage,dmage2,female]).T
Y = np.array(data.dbirwt).reshape(n,1)

# have the guesses for beta be the OLS estimates
betaguess = beta

def QTreg_solve(betaguess, tau, Y, X):
    u = Y - (np.dot(X,betaguess)).reshape(n,1)
    # here is the tilted absolute value function
    u_pos = np.where(u>0)[0]
    u_neg = np.where(u<=0)[0]
    rho_pos = u[u_pos]*tau 
    rho_neg = u[u_neg]*(1-tau)

    return rho_pos.sum() + np.abs(rho_neg.sum())

tau = .5

"""
QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)
beta_min = spicy.fmin(QTsolve,betaguess)
print beta_min
"""
##################################
from matplotlib import pyplot as plt
nq = 19
quantiles = np.linspace(.05,.95,nq)
coeffs = np.zeros((nq,len(betaguess)))
success = np.zeros(nq)

for i in range(nq):
    tau = quantiles[i]
    QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)
    powell = spicy.minimize(QTsolve,betaguess,method='Powell')
    #beta_min = spicy.fmin(QTsolve,betaguess)
    coeffs[i] = powell.x #beta_min
    success[i] = powell.success

#graph
variables = ['constant','marriage','black','otherrace','dmeduc','dmage','dmage2','female']
for i in range(8):    
    plt.plot(quantiles,coeffs[:,i])
    plt.title("Coeff. of "+variables[i]+" by quantile")
    plt.savefig("byQuantile_"+variables[i]+".png")
    plt.show()

print success


"""
# Experimenting with a few different minimizers-
tau = .5
QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)

beta_min = spicy.fmin(QTsolve,betaguess)#nope
bmin = spicy.minimize(QTsolve,betaguess)#nope
NM = spicy.minimize(QTsolve,betaguess,method='Nelder-Mead')#nope
LBFSG = spicy.minimize(QTsolve,betaguess,method='L-BFGS-B')#yes
powell = spicy.minimize(QTsolve,betaguess,method='Powell')#yes
BFGS = spicy.minimize(QTsolve,betaguess,method='BFGS')#nope
pow_min = spicy.fmin_powell(QTsolve,betaguess)# yes

print LBFSG.x, LBFSG.fun
print powell.x, powell.fun

It looks like the Powell is the better option of the two that actually
converged, its function value is less.
"""


data['num'] = 1
pd.pivot_table(data,values='bldstest',columns=['num'],aggfunc=np.sum)

pd.pivot_table(data,values='bldstest',index=['num'],aggfunc=np.sum)




# Timing them
import time
tau = .5
QTsolve = lambda betaguess: QTreg_solve(betaguess, tau, Y, X)

t1L = time.time()
LBFSG = spicy.minimize(QTsolve,betaguess,method='L-BFGS-B')#yes
totalL = time.time()-t1L
print LBFSG
print "The LBFSG took ", totalL
t1P = time.time()
powell = spicy.minimize(QTsolve,betaguess,method='Powell')#yes
totalP = time.time()-t1P
print powell
print "The Powell took ", totalP

""" So it looks like the Powell takes a lot longer than the L-BFGS-B,
but it is more precise """




