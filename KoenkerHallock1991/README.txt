An explanation of the files in here-

Of the KoenkerHallock1991 files, v2, v3, and v5 are the important ones

v2 - Graphs the coefficients of each variable by quantile for the regular 
quantile regression, with no fixed effects included.

v3 - Graphs the objective funtion for each variable with the expanded model,
included state and time fixed effects and a quadratic interaction term between the two.

v5 - This contains the Quantile Instrumental Variables estimator, and code that will graph each coefficient by quantile