""" Replication of Angrist & Krueger (1991) - Alex Poulsen"""


import numpy as np
from numpy import linalg

from pandas import Series, DataFrame, Index 
import pandas as pd 

import os 

os.chdir("/Users/Alex/Economics/AngristKrueger1991")
 
# Import the data
data = pd.io.parsers.read_table("asciiqob.txt",sep="         ", names = ["lwklywage","educ","yob","qob","pob"])

# OLS 
n = data.shape[0]
X = np.array([np.ones(n),data.educ]).T
Y = np.array(data.lwklywage).reshape(n,1)
a = linalg.inv(np.dot(X.T,X))
b = np.dot(X.T,Y)
beta = np.dot(a,b)
#print "My code gives a coefficient vector of: ", beta
uhat = Y-beta[0]-(beta[1][0]*X[:,1]).reshape(n,1)
sigma2 = (1./(n-2))*np.dot(uhat.T,uhat)
varcovar = sigma2[0,0]*a
#print "with standard errors: ", np.array([varcovar[0,0]**.5, varcovar[1,1]**.5])

print "Using my python code, I get the following results for OLS: "
print "b0 = ", beta[0,0], "/ s.e.(b0) = ", varcovar[0,0]**.5
print "b1 = ", beta[1,0], "/ s.e.(b1) = ", varcovar[1,1]**.5


reg = pd.ols(y = data.lwklywage, x = data[['educ']])
print "Pandas' built in OLS function gives me: "
print reg

# IV
data['frstqrtr'] = data.qob == 1 # this functions as a dummy variable

X = np.array([np.ones(n),data.educ]).T
Y = np.array(data.lwklywage).reshape(n,1)
Z = np.array([np.ones(n),data.frstqrtr]).T
a = linalg.inv(np.dot(Z.T,X))
b = np.dot(Z.T,Y)
beta = np.dot(a,b)
print "My code gives a coefficient vector of: ", beta

""" to find var/covar matrix...
SSTx = np.dot(X[:,1].T,X[:,1])
SSTz = np.dot(Z[:,1].T,Z[:,1])
uhat = Y-beta[0]-(beta[1][0]*X[:,1]).reshape(n,1)
sigma2 = (1./(n-2))*np.dot(uhat.T,uhat)
"""





