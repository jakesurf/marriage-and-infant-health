# README #

This repository is for my code for the Marriage and Infant Health Project I am working on with Dr. Price and Dr. Frandsen.

The main purpose of this repository is to keep things up to date between my office computer and personal computer, but it will also be good for sharing code, graphs, and other results.